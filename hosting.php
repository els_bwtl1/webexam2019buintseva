<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-left">
    <h1 class="display-4">Хостинг</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-eco-tab" data-toggle="pill" href="#pills-eco" role="tab" aria-controls="pills-eco" aria-selected="true">Эконом</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-fast-tab" data-toggle="pill" href="#pills-fast" role="tab" aria-controls="pills-fast" aria-selected="false">Скоростные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-vip-tab" data-toggle="pill" href="#pills-vip" role="tab" aria-controls="pills-vip" aria-selected="false">VIP</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-eco" role="tabpanel" aria-labelledby="pills-eco-tab">
                    <div class="card-deck mb-3 text-center">
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-Lite</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">7 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>15 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 83Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-A</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">7 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>1 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 95Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-B</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">25 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>1 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 179Руб./мес.</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-fast" role="tabpanel" aria-labelledby="pills-fast-tab">
                    <div class="card-deck mb-3 text-center">
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-0</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">12 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>7 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 129Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-1</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">17 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>15 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 210Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">Host-3</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">27 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>30 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 324Руб./мес.</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-vip" role="tabpanel" aria-labelledby="pills-vip-tab">
                    <div class="card-deck mb-3 text-center">
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">VIP-1</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">12 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>7 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 539Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">VIP-2</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">17 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>15 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 659Руб./мес.</button>
                            </div>
                        </div>
                        <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal">VIP-3</h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title">27 ГБ SSD</h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li>30 сайтов и ? БД</li>
                                    <li>? псевдонимов</li>
                                    <li>? трафик</li>
                                    <li>ISPmanager</li>
                                    <li>Plesk</li>
                                    <li>CPanel</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary">От 959Руб./мес.</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-link">Больше скорости и мощности</button>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">+ Домен</a></li>
        </ol>
    </nav>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">+ Дополнительный IP-адрес</a></li>
            <li class="breadcrumb-item active" aria-current="page">79руб./мес.</li>
        </ol>
    </nav>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">+ Подписка на антивирус для сайтов</a></li>
            <li class="breadcrumb-item active" aria-current="page">259руб./мес.</li>
        </ol>
    </nav>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">+ Бесплатный SSL-сертификат</a></li>
            <li class="breadcrumb-item active" aria-current="page">Бесплатно на 1 год</li>
        </ol>
    </nav>
</div>
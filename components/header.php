<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark d-flex align-items-center">
        <a class="navbar-brand text-light my-0" href="/">Московский политех</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon text-light"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav text-light d-flex align-items-center ml-auto">
                <a class="nav-item nav-link text-light py-0" href="/hosting">Хостинг</a>
                <a class="nav-item nav-link text-light py-0" href="#">Регистрация</a>
                <a class="nav-item nav-link text-light btn btn-primary px-5 py-0" href="#">Войти</a>
            </div>
        </div>
    </nav>
</header>
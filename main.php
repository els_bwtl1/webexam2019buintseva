<section class="mb-4">
    <div class="col-12">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://mospolytech.ru/photo/img_100_1388126698.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://mospolytech.ru/photo/img_46_1522238094.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://mospolytech.ru/photo/img_100_1388126698.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="row d-flex flex-column flex-md-row justify-content-between">
            <div class="col-12 col-md-4">
                <div class="card" style="width: 15rem;">
                    <img class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Хостинг и серверы</h5>
                        <p class="card-text">Надежный классический и VIP хостинг. VPS с SSD+HDD и SSD носителми, Dedicated. Доменя в подарок</p>
                        <div class="d-flex align-items-center">
                            <a href="#" class="btn btn-primary mr-2">Подробнее</a>
                            <p class="mb-0">от 91 руб.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card" style="width: 15rem;">
                    <img class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Хостинг и серверы</h5>
                        <p class="card-text">Надежный классический и VIP хостинг. VPS с SSD+HDD и SSD носителми, Dedicated. Доменя в подарок</p>
                        <div class="d-flex align-items-center">
                            <a href="#" class="btn btn-primary mr-2">Подробнее</a>
                            <p class="mb-0">от 91 руб.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card" style="width: 15rem;">
                    <img class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Хостинг и серверы</h5>
                        <p class="card-text">Надежный классический и VIP хостинг. VPS с SSD+HDD и SSD носителми, Dedicated. Доменя в подарок</p>
                        <div class="d-flex align-items-center">
                            <a href="#" class="btn btn-primary mr-2">Подробнее</a>
                            <p class="mb-0">от 91 руб.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="row flex-column flex-md-row">
            <div class="col-12 col-md-7 d-flex flex-column p-2 mb-2 mb-md-0" style="border: 5px solid grey">
                <h2>Новости</h2>
                <p class="bg-light py-2 px-4">13 инюня 2019 г.</p>
                <p class="py-2 px-4">Скидка 50% на услугу...</p>
                <p class="bg-light py-2 px-4">13 инюня 2019 г.</p>
                <p class="py-2 px-4">Скидка 50% на услугу...</p>
                <p class="bg-light py-2 px-4">13 инюня 2019 г.</p>
                <p class="py-2 px-4">Скидка 50% на услугу...</p>
                <p class="bg-light py-2 px-4">13 инюня 2019 г.</p>
                <p class="py-2 px-4">Скидка 50% на услугу...</p>
            </div>
            <div class="col-12 col-md-4 offset-md-1 d-flex flex-column p-2 mb-2 mb-md-0" style="border: 5px solid grey">
                <h2>Мероприятия</h2>
                <p class="bg-light py-2 px-4">19 инюня, Москва</p>
                <p class="py-2 px-4">Скидка 50% на услугу...</p>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <h1 class="text-center mb-4">Почему мы это просто?</h1>
        <div class="d-flex flex-column align-items-center justify-content-center flex-md-row flex-md-wrap">
            <div class="d-flex flex-column block mr-md-2">
                <div class="bordered">
                    №1
                </div>
                <p class="text-center">регистратор и хостинг-провайдер России</p>
            </div>
            <div class="d-flex flex-column block mr-md-2">
                <div class="bordered">
                    №1
                </div>
                <p class="text-center">регистратор и хостинг-провайдер России</p>
            </div>
            <div class="d-flex flex-column block mr-md-2">
                <div class="bordered">
                    №1
                </div>
                <p class="text-center">регистратор и хостинг-провайдер России</p>
            </div>
            <div class="d-flex flex-column block mr-md-2">
                <div class="bordered">
                    №1
                </div>
                <p class="text-center">регистратор и хостинг-провайдер России</p>
            </div>
            <div class="d-flex flex-column block mr-md-2">
                <div class="bordered">
                    №1
                </div>
                <p class="text-center">регистратор и хостинг-провайдер России</p>
            </div>
        </div>
    </div>
</section>